public class Ruut {
    public static int vanus = 5;

    public static int getVanus(){
        return vanus;
    }

    public static double ruut(double a){
        return a*a;
    }

    public static int korrutaja (int c, int b) {  //Refactor - koodi muutmine
        return c * b;
    }

    public int astendus(int a, int b) {
        return (int) Math.pow(a, b);
    }

    public void korrutamineVanusega(int a) {
        vanus = vanus * a;
    }

}
