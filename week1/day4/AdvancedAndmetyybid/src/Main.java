import java.util.ArrayList;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        // Array ehk massiiv
        int[] massiiv = new int[6]; //primitiiv (kiirem)
        ArrayList list = new ArrayList(); //object, rohkem võimalusi

        //Ül: prindi välja massiiv
        //System.out.println(massiiv);
        String massiivStr = Arrays.toString(massiiv);
        //System.out.println(massiivStr);

        //Ül: muuda kolmandal positsioonil olev number viieks
        massiiv[2] = 5;
        //System.out.println(Arrays.toString(massiiv));

        //Ül: määra väärtust ja prindi välja viimane element massiivist
        massiiv[5] = 9;
        //System.out.println(massiiv[5]);

        //Ül: prindi välja viimane element, ükskõik kui pikk massiiv ka ei oleks.
        int viimane = massiiv[massiiv.length - 1]; //massiiv.length = array pikkus. -1, sest algab nullist.(üks koht tagasi)
        //System.out.println("viimane: " + viimane);

        //Ül: loo uus massiiv, kus on kõik 8 numbrit kohe määratud
        int[] massiiv2 = new int[] {3,8,65,346,4,2,654,54};
        //System.out.println(Arrays.toString(massiiv2));

        //Ül: prindi välja ükshaaval kõik väärtused massiiv2'st
        int index = 0;
        while (index < massiiv2.length){
            //System.out.println(massiiv2[index]);
            index++;
        }

        //Ül: sama tsükkel kiiremini for tsükli abil
        for (int i = 0; i < massiiv2.length; i++) {
            //System.out.println(massiiv2[i]);
        }

        //Ül: loo Stringide massiiv, mis on alguses tühi, hiljem lisad keskele sõne + prindi välja
        String[] strStr = new String[3];
        //System.out.println(Arrays.toString(strStr));
        strStr[1] = "Tere!";
        //System.out.println(strStr[1]);

        //Ül: loo massiiv, kus on 100 kohta. Sisesta sinna loetelu numbreid 0..99 + prindi välja
        int[] loetelu = new int[100];
        for (int i = 0;  i< loetelu.length; i++) {
            loetelu[i] = i;
        }
        //System.out.println(Arrays.toString(loetelu));

        //Ül: kasuta loetelu massiivi, kus on nr jada, loe mitu paarisarvu on
        int count = 0;
        for (int i = 0; i < loetelu.length; i++) {

            if(loetelu[i]% 2 == 0){
                count++;
            }
        }
        // System.out.println(count);


        //Ül: loo ArrayList ja sisesta sinna 3 int't ja 2 Stringi

        ArrayList jada = new ArrayList();
        jada.add(13);
        jada.add(52);
        jada.add(6);
        jada.add("Esimene");
        jada.add("Teine");

        // System.out.println(jada);

        //Ül: Küsi vimaseest ArrayListist välja kolmas element ja prindi välja

        // System.out.println(jada.get(2));

        //Ül: prindi iga element ükshaaval
        for (int i = 0; i < jada.size(); i++) {
            // System.out.println(jada.get(i));
        }

        //Ül: Loo uus ArrayList, kus on 543 suvalist numbrit vahemikus 0-10.
        // Korruta iga nri 5'ga ja salvesta uus nr samale postitsioonile.



        ArrayList<Integer> numbrid = new ArrayList<Integer>();  //võib Integer vahelt ära jätta <>
        ArrayList<Integer> kordaViis = new ArrayList<Integer>();


        for (int i = 0; i < 543; i++) {
            double nr = Math.random() * 11;
            numbrid.add((int)nr);
            kordaViis.add((int)nr*5);
        }
        System.out.println(numbrid);
        System.out.println(kordaViis);
<<<<<<< HEAD
        

=======

        ///

        ArrayList massiiv543 = new ArrayList();
        for (int i = 0; i < 543; i++) {
            int nr = (int)(Math.random() * 11);
            //System.out.println(nri);
            massiiv543.add(nr);
        }
        for (int i = 0; i < massiiv543.size(); i++) {
            double nr = (double) massiiv543.get(i);
            double muudetudNr = (nr * 5);
            massiiv543.set(i, muudetudNr);
        }
        
>>>>>>> bacb74d77dc2a0591c9451cc26ef3d1e436138dd
    }
}

