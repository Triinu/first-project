// esimene
console.log("teisip2eva hommik!!")

// teine (if)
var sisend = 7
var tulemus

if(sisend < 7) {
	tulemus = sisend*2
}
else if (sisend > 7) {
	tulemus = sisend/2
} else {
	tulemus = sisend
}
console.log("Tulemus on: " + tulemus)

// kolmas (if string)
var str1 = "banaan"
var str2 = "apeslin"

if(str1 == str2) {
	console.log(str1)
} else {
	console.log(str1+ " " + str2)
}

// neljas (while)
var linnad = ["Tallinn", "Tartu", "Valga"]
var uuedLinnad = []

while (linnad.length > 0) { //võtab kuni linnasid on listis
	var linn = linnad.pop() //pop võtab listist viimase 
	var uusLinn = linn + " linn" 
	uuedLinnad.push(uusLinn) //push salvestab uue tulemuse
}
console.log(uuedLinnad)

// viies (while + if)
var nimed = ["Margarita", "Mara", "Martin", "Kalev"]
var poisteNimed = []
var tydrukuteNimed = []

while(nimed.length > 0) {
	var nimi = nimed.pop()
	if(nimi.endsWith("a")){
		tydrukuteNimed.push(nimi)
	} else {
		poisteNimed.push(nimi)
	}
}
console.log("Poiste nimed on: " + poisteNimed + " Tüdrukute nimed on: " + tydrukuteNimed)

// kuues (funktsioon)
var eristaja = function(nimi) {
	if(nimi.endsWith("a")){
		return "tüdruk"
	} else {
		return "poiss"
	}
}
var praeguneNimi = "Peeter"
var kumb = eristaja(praeguneNimi)

console.log(kumb)

/* seitsmes 
!isNaN(4) - is not a number, reverse by "!" = is number
*/
var kasOnNumber = function(number) {
	if(!isNaN(number)) {
		return true
	} else {
		return false
	}

}
console.log(kasOnNumber(4))
console.log(kasOnNumber("mingi sõne"))
console.log(kasOnNumber(23536))
console.log(kasOnNumber(6.876))
console.log(kasOnNumber(null))
console.log(kasOnNumber([1, 4, 5, 6]))

// kaheksas võta kaks parameetrit ja liida kokku
var liida = function(a, b) {
	return a + b 
}

console.log(liida(1, 2))
console.log(liida(19, 32))

console.log("--J2RGMINE YLESANNE--")
// üheksas
var inimesed = {
	"kaarel": 34,
	"margarita": 10,
	"ratsu": [3, 6, 8],
	"suksu": {
		vanus: 5,
		sugu: true
	}
}
console.log(inimesed["kaarel"])
console.log(inimesed.kaarel)
console.log(inimesed.suksu.sugu)
console.log(inimesed.ratsu[2])