// esimene ülesanne - alla laadida APIst tekst
var refreshMessages = async function() {
	console.log("refreshMessages läks käima")
	//API aadress on string, salvestan lihtsalt muutujasse
	var APIurl = "http://138.197.191.73:8080/chat/general"
	//fetch teeb päringu serverisse (meie defineeritud aadress)
	var request = await fetch(APIurl)
	//jason() käsk vormindab meile data mugavaks jsoniks
	var json = await request.json()
	// document.querySelector('#jutt').innerHTML = JSON.stringify(json)
	//kuva serverist saadud info HTMLis
	document.querySelector('#jutt').innerHTML = ""
	var sonumid = json.messages

	while(sonumid.length > 0) {
		var sonum = sonumid.shift()
		// console.log(sonum)
		// lisa HTMLi #jutt sisse sonum.message
		document.querySelector('#jutt').innerHTML += "<p>" + sonum.user + " ütleb: " + sonum.message + "</p>"
	}

	window.scrollTo(0, document.body.scrollHeight);
}
setInterval(refreshMessages, 1000)

//
document.querySelector('form').onsubmit = function(e) {
	event.preventDefault()
	var username = document.querySelector('#username').value
	var message = document.querySelector('#message').value
	document.querySelector('#message').value = " " //tühjenda input

	// console.log(username, message)
	// document.querySelector('#jutt').innerHTML += "<br>" + username + " kirjutas: " + message

	//post pärring postitab uue andmetüki serverisse
	var APIurl = "http://138.197.191.73:8080/chat/general/new-message" //serveri poolt antud url
	fetch(APIurl, {
		method: "POST",
		body: JSON.stringify({user: username, message: message}),
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/json'
		}
	})
}