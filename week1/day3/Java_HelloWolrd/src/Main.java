public class Main {

    public static void main(String[] args) {
        System.out.println("Hello World!");
        int number = 5; //täisarv
        double nr2 = 5.1;//komakohaga arv
        float n3 = 5.6f;
        byte bait = 7;
        long nr4 = 57493894; //vääääga pikk number

        String nimi = "Triinu"; //sõne
        nimi.length();
        char algusT2ht = 'T'; //üks sümbol



        if(number == 5) { //if süntaks
        }else {
        }

        int liitmine = (int) (number + nr2); //cast-in ühest tüübist teise
        System.out.println(liitmine);
        System.out.println(Math.round(10.7)); //ümardamiseks eraldi meetod

        int parisNumber = Integer.parseInt("4"); //muudab Stringi int'iks
        double parisNumber2 = Double.parseDouble("4.4"); //muudab Stringi double'ks


        String puuvli1 = "Banaan";
        String puuvli2 = "Apelsin";

        if(puuvli1 == puuvli2) { //turvaline on võrrelda Stringe equals'i abil
            System.out.println("Puuvlid on võrdsed");
        } else {
            System.out.println("Ei ole võrdsed!");
        }

        Koer.auh();
        Koer.lausu();
        Kass.nurr();
    }
    public static int summa(int a, int b) {
        return a + b;
    }
}
