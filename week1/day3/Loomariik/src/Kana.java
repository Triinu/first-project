public class Kana extends Koduloom {
    @Override
    public void lausu() {
        System.out.println("kaaaaaak-pagaaak-kaaak!");
    }

    @Override
    public void ajaPeremeesYles() {
        System.out.println("KAAAAAK");
    }

    public String getSaba(){
        return "Mul on ainuld mingid suled seal";
    }
}
