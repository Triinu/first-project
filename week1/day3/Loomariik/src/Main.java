public class Main {

    //lühend "psvm"
    public static void main(String[] args) {
        Koer pontu = new Koer();
        pontu.lausu();
        pontu.maga();
        pontu.ajaPeremeesYles();
        System.out.println(pontu.getSaba());

        Kass nurr = new Kass();
        nurr.lausu();
        nurr.maga();
        nurr.ajaPeremeesYles();
        System.out.println(nurr.getSaba());

        Kana tiiu = new Kana();
        tiiu.lausu();
        tiiu.maga();
        tiiu.ajaPeremeesYles();
        System.out.println(tiiu.getSaba());
    }
}
