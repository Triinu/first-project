import java.util.Random;

public class Suusataja {

    int stardiNumber;
    double kiirus = 1;
    double labitudDistants;
    private double dopinguKordaja;



    public Suusataja(int i) {
        stardiNumber = i;
        kiirus = getRandomNumberInrange(10,20);
        labitudDistants = 0;
        this.dopinguKordaja = Math.random();
        /*if (i % 4 == 0) {  //kasutab dopingut
            kiirus = kiirus + 10;
            kahtlustus++;
        }*/
    }

    public String getDopinguKordaja() {
        if(this.dopinguKordaja < 0.5) {
            return "dopingukontroll oli negatiivne";
        }
        return "dopingukontrollis oli A-proov positiivne";
    }

    public static double getRandomNumberInrange(int min, int max) {
        if(min >= max) {
            throw new IllegalArgumentException("Max must be greater than min");

        }
        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;

    }


    public void suusata() {
        if (this.dopinguKordaja < 0.5) {
            this.labitudDistants += this.kiirus/3600;
        } else {
            this.labitudDistants += (this.kiirus/3600) *1.15;
        }
        //labitudDistants += kiirus/3600;
    }

    public String toString(){
        int dist = (int)(labitudDistants *1000);
        return "Osaleja number " + stardiNumber+" : "+dist;
    }



    public boolean kasOnLopetanud(int koguDistants) {
        return labitudDistants >= koguDistants;
    }

}
