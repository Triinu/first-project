import weekdays.Laupaev;
import weekdays.Puhapaev;
import weekdays.Reede;

public class Main {

    public static void main(String[] args) {

        // System.out.println("Hello World!");

        if (false || false && true || true) {
            System.out.println("Tõene");
        } else {
            System.out.println("Väär");
        }

        //reede on klass, koju on meetod.
        //klassi ja  meetodi loomise shortcut: klikka peale ja Alt+Enter

        //Reede.koju();

        Laupaev.peole();

        Puhapaev.hommik();

        Puhapaev day = new Puhapaev();

        day.maga();
        day.hommik();


    }
}
