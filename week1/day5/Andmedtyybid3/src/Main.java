import java.util.ArrayList;
import java.util.HashMap;

public class Main {
    public static void main(String[] args) {

        //Ül:
        // Loo kolm muutujat numbritega
        // Moodusta lause nende muutujatega
        // Prindi lause välja

        int aasta = 92;
        int kuu = 6;
        int day = 4;
        String lause = "Ma sündisin aastal " +aasta+ ". Kuu oli " +kuu+ " ja päev " +day+".";
        // -----System.out.println(lause);

        String parem = String.format("Mina sündisin aastal %d. Kuu oli %d ja päev %d.", aasta, kuu, day);
        //%d - täisarv
        //%d - komakohaga
        //%s - String
        //format -
        // -----System.out.println(parem);


        //TEEMA: HashMap

        //Ül: loe kokku mitu lampi on klassis ja pane see info HashMapi.
        //Ül: loe kokku mitu akent on klassis ja pane see inho HashMapi.
        //Ül: loe kokku mitu inimest on klassis ja pane see info HashMapi.

        HashMap klassiAsjad = new HashMap(); //võti ja väärtus -> ei ole järjekorda, esitab kujul, kuidas info
        // mällu salvestub. sama võtme salvestamisel uuendab väärtust, ei prindi kahte ühesugust võtit erneva väärtusega

        klassiAsjad.put("aknad", 5);
        klassiAsjad.put("lambid", 11);
        klassiAsjad.put("inimsed", 21);
        // -----System.out.println(klassiAsjad);


        // mõtle välja 3 praktilist kasutust HashMapile
        // 1 - lasteaias on {lapseID; kohalkäimiste arv}
        // 2 - kohvikus on {toit; hind}
        // 3 - veebilehekülastus {kasutajaID; sessiooni kestus}

        //Ül: prindi välja kui palju on inimesi klassis, kasutades juba loodud HashMapi.

        // -----System.out.println("Klassis on : " +klassiAsjad.get("inimsed")+ ".");

        //Ül: lisa samasse HashMapi juurde mitu tasapinda on klassis, aga nr enne ja siis String
        klassiAsjad.put(10, "tasapinnad");
        // -----System.out.println(klassiAsjad);

        //Ül: loo uus HashMap, kuhu saab sisestada AINULT String: double paare. Sisesta midagi

        HashMap<String, Double> uusMap = new HashMap<>();
        uusMap.put("Vanus", 5.0);
        uusMap.put("Kell", 11.22);

        //TEEMA: switch - kasutada if asemel, kui on vaja sisestada palju erinevaid variante
        //Ül

        int rongiNr = 555;
        String suund = null;
        switch (rongiNr) {
            case 50: suund = "Pärnu";
            break;
            case 55: suund = "Haapsalu";
            break;
            case 10: suund = "Vormsi";
            break;
            default: suund = "Seisab Tallinnas";
        }
        //System.out.println(suund);

        //TEEMA: forEach

        int[] mingidNumbrid = new int[]{8, 4, 6, 345, 8978, 12345};
        for (int i = 0; i < mingidNumbrid.length; i++) {
            // -----System.out.println(mingidNumbrid[i]);
        }
        for (int nr: mingidNumbrid){
            System.out.println(nr);
        }

        //Ül: switch - õpilane saab töös punkte 0-100
        //Kui punkte on alla 50 - kukub läbi, hinne (int) punktid/20
        //100 puntki => 5
        //80 puntki => 4
        //67 puntki => 3
        //100 puntki => 5

        int punkte = 84;
        if(punkte>100 || punkte <0){
            throw new Error();
        }
        switch ((int)Math.round(punkte/20.0)){
            case 5:
                System.out.println("Väga hea");
                break;
            case 4:
                System.out.println("Hea");
                break;
            case 3:
                System.out.println("Rahuldav");
                break;
            case 2:
                System.out.println("eee... õppisid ka?");
                break;
            default:
                System.out.println("Kukkusid läbi");
        }





    }
}
