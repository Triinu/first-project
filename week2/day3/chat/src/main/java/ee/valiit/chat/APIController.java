package ee.valiit.chat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;

@RestController
@CrossOrigin
public class APIController {

    @Autowired  //ühendab automaatselt
            JdbcTemplate jdbcTemplate;


    @GetMapping("/chat/{room}")
        //SELECT * FROM- tagastab ridu andmebaasist
    ArrayList<ChatMessage> chat(@PathVariable String room) {
        try{
        String sqlKask = "SELECT * FROM messages WHERE room='"+room+"'";
        ArrayList<ChatMessage> messages =
                (ArrayList) jdbcTemplate.query(sqlKask, (resultSet, rowNum) -> {  //peab tõlgendama read loetavaks
            String username = (resultSet.getString("username"));
            String message = (resultSet.getString("message"));
            String url = (resultSet.getString("url"));
            int id = (resultSet.getInt("id"));
            return new ChatMessage(username, message, url, room, id);
        });
    return messages;
        } catch (DataAccessException err){   //guery viskab seda errorit(dataAccessException), err on nimi
            System.out.println("TABLE WAS NOT READY");
            return  new ArrayList();

        }
    }

    @PostMapping("chat/{room}/new-message")
    void newMessages(@RequestBody ChatMessage msg, @PathVariable String room) {
        String sqlKask = "INSERT INTO messages (username, message, url, room) VALUES ('"+
                msg.getUsername()+"', '"+
                msg.getMessage()+"', '"+
                msg.getUrl()+"', '"
                +room+"')";
        jdbcTemplate.execute(sqlKask);
    }
}