package ee.valiit.chat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.JdbcTemplate;

@SpringBootApplication
public class ChatApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(ChatApplication.class, args);
	}

	@Autowired  //käsk - ühendab automaatselt
	JdbcTemplate jdbcTemplate;


	@Override
	public void run(String... args) throws Exception {
		System.out.println("Configure database tables");
		jdbcTemplate.execute("DROP TABLE IF EXISTS messages");
		jdbcTemplate.execute("CREATE TABLE messages (id SERIAL, username TEXT, message TEXT, url TEXT, room TEXT)");

	}
}