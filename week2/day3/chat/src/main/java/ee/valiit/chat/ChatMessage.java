package ee.valiit.chat;

public class ChatMessage {
    private int id;
    private String username;
    private String room;
    private String message;
    private String url;

    public ChatMessage() {

    }

    public ChatMessage(String username, String message, String url, String room, int id) {
        this.username = username;
        this.message = message;

    }

    public ChatMessage(String username, String message, String url) {
        this.username = username;
        this.message = message;
        this.url = url;
    }



  public ChatMessage(int id, String username, String room, String message){
        this.id = id;
        this.username = username;

        this.url = url;

        this.room = room;
        this.id = id;


    }

    public int getId() {

        return id;
    }

    public String getUsername() {

        return username;
    }

    public String getRoom() {

        return room;
    }

    public String getMessage() {

        return message;
    }

    public String getUrl() {
        return url;
    }

}
