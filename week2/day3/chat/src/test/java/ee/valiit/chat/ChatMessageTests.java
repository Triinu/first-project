package ee.valiit.chat;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ChatMessageTests {

    @Test
    public void test1(){
        ChatMessage cm = new ChatMessage("kasutaja", "testin, testin 123", "");
        assertEquals("kasutaja", cm.getUsername());  //võrdleb kahte tulemust, "kasutaja", eeldatav tulemus, getUsername on saadav tulemus, võtdleb neid omavahel
        assertEquals("testin, testin 123", cm.getMessage());

    }

}
