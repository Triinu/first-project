import java.math.BigInteger;

public class Main {

    public static void main(String[] args) throws Exception {

        //Ül1:
        System.out.println(Harjutused.test(4));
        //Ül2:
        Harjutused.test2("Hello, ", "World");
        //Ül3:
        System.out.println(Harjutused.addVat(5.8));
        //Ül4:
        System.out.println(Harjutused.newInt(13,15, true));
        //Ül5:
        System.out.println(Harjutused.deriveGender("49206046035"));
        String gender = Harjutused.deriveGender("49206046035");
        System.out.println("Gender is: " +gender);
        //Ül6:
        Harjutused.printHello();
        //Ül7:
        System.out.println(Harjutused.deriveBirthYear("49206046035"));
        //Ül8:
        BigInteger isikukoodNr = new BigInteger("49206046035");
        boolean yl8 = Harjutused.validatePersonaolCode(isikukoodNr);
        System.out.println(Harjutused.validatePersonaolCode(isikukoodNr));




    }


}
