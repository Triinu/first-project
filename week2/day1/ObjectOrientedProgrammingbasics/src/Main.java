import java.util.Arrays;

public class Main {
    public static void main(String[] args) {

        Skydiver first = new Skydiver();
        Skydiver second = new Skydiver();
        Skydiver third = new Skydiver();

        Runner first1 = new Runner();
        Runner second1 = new Runner();
        Runner third1 = new Runner();

        first.firstName = "Mike";
        first.lastName = "Smith";
        first.age = 13;
        first.gender = "mees";
        first.length = 145.6;
        first.weight = 45.7;

        second.firstName = "July";
        second.lastName = "Toe";
        second.age = 20;
        second.gender = "naine";
        second.length = 180.0;
        second.weight = 58;

        third.firstName = "Peeter";
        third.lastName = "Eeter";
        third.age = 22;
        third.gender = "mees";
        third.length = 195.2;
        third.weight = 78.9;

        first1.firstName = "Paul";
        first1.lastName = "Saul";
        first1.age = 10;
        first1.gender = "mees";
        first1.length = 119.6;
        first1.weight = 30.0;

        second1.firstName = "Mari";
        second1.lastName = "Maasikas";
        second1.age = 18;
        second1.gender = "naine";
        second1.length = 163.2;
        second1.weight = 50.0;

        third1.firstName = "Helgi";
        third1.lastName = "Telgi";
        third1.age = 74;
        third1.gender = "naine";
        third1.length = 157.4;
        third1.weight = 54.6;

        System.out.println(first.firstName+" "+first.lastName+ " vanus: "+first.age+" sugu: "+first.gender+" pikkus: "+first.lastName+" kaal: "+first.weight);
        System.out.println(second.firstName+" "+second.lastName+ " vanus: "+second.age+" sugu: "+second.gender+" pikkus: "+second.lastName+" kaal: "+second.weight);
        System.out.println(third.firstName+" "+third.lastName+ " vanus: "+third.age+" sugu: "+third.gender+" pikkus: "+third.lastName+" kaal: "+third.weight);
        System.out.println(first1.firstName+" "+first1.lastName+ " vanus: "+first1.age+" sugu: "+first1.gender+" pikkus: "+first1.lastName+" kaal: "+first1.weight);
        System.out.println(second1.firstName+" "+second1.lastName+ " vanus: "+second1.age+" sugu: "+second1.gender+" pikkus: "+second1.lastName+" kaal: "+second1.weight);
        System.out.println(third1.firstName+" "+third1.lastName+ " vanus: "+third1.age+" sugu: "+third1.gender+" pikkus: "+third1.lastName+" kaal: "+third1.weight);

        first.perform();
        second.perform();
        third.perform();
        first1.perform();
        second1.perform();
        third1.perform();

    }
}
