import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {

        ArrayList<Athlete> sportlased = new ArrayList<>();
        sportlased.add(new Skydiver("Peeter","Kask"));
        sportlased.add(new Skydiver("Tarmo","Tamm"));
        sportlased.add(new Skydiver("Linda","Paju"));
        sportlased.add(new Runner("Mari", "Oja"));
        sportlased.add(new Runner("Paul", "Pikk"));
        sportlased.add(new Runner("Anne", "Oks"));

        for (Athlete sportlane: sportlased){
            System.out.println(sportlane.eesnimi+" "+sportlane.perenimi);
        }

    }
}
