public class Skydiver extends Athlete {
    public Skydiver(String eesnimi, String perenimi) {
        super(eesnimi, perenimi);
    }

    @Override
    public void perform() {
        System.out.println(eesnimi +" "+ perenimi +": parem, vasak, parem");
    }
}
