public abstract class Athlete {

    String eesnimi;
    String perenimi;
    int vanus;
    String sugu;
    double pikkus;
    double kaal;

    public abstract void perform();  // abstract - annab ette pooliku klassi, saab täiendada.
    // Abstact meetodit peab extendides kindlasti täiendama

    public Athlete(String eesnimi, String perenimi){
        this.eesnimi = eesnimi;
        this.perenimi = perenimi;
    }
}
