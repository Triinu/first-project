public class Runner extends Athlete {
    public Runner(String eesnimi, String perenimi) {
        super(eesnimi, perenimi);  // super on ainult constructori sees, kui tahame kasutada parent constructorit
    }

    @Override
    public void perform() {
        System.out.println(eesnimi + " " +perenimi+ ": langen, langen, langen");
    }
}
