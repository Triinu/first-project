package ee.valiit.chat;

import java.util.ArrayList;

public class ChatRoom {
    //klassi väljad
    public String room;
    public ArrayList<ChatMessage> messages = new ArrayList();

    //constructor
    public ChatRoom(String room){
        this.room = room;  //this viitab sellele klassile
        messages.add(new ChatMessage("kasutaja", "proov", "https://www.what-dog.net/Images/faces2/scroll000.jpg"));
    }


    public void addMessage(ChatMessage msg) {

        messages.add(msg);
    }
}
