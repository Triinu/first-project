package ee.valiit.chat;

import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

@RestController
@CrossOrigin  //pole turvaline, lubab kõik muudatused
public class APIController {
    //ChatRoom general = new ChatRoom("general");
    //ChatRoom random = new ChatRoom("random");
    HashMap<String, ChatRoom> rooms = new HashMap();


    public APIController(){
        rooms.put("general", new ChatRoom ("general"));
        rooms.put("random", new ChatRoom ("random"));
        rooms.put("materjalid", new ChatRoom("materjalid"));
    }


    @GetMapping("/chat/{room}")
    ChatRoom chat(@PathVariable String room) {

        return rooms.get(room);
    }

    @PostMapping("chat/{room}/new-message")
    void newMessages (@RequestBody ChatMessage msg, @PathVariable String room){

        rooms.get(room).addMessage(msg);
    }



/*
    @GetMapping("/chat/random")
    ChatRoom rand() {

        return random;
    }

    @PostMapping("chat/random/new-message")
    void newRandMessages(@RequestBody ChatMessage msg) {

        this.random.addMessage(msg);
    }*/

}
