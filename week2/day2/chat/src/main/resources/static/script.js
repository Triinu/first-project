// esimene ülesanne - alla laadida APIst tekst
var refreshMessages = async function() {
	console.log("refreshMessages läks käima")
	//Uus muutuja, kuhu salvestame praeguse toa
    var tuba = document.querySelector("#room").value
	//API aadress on string, salvestan lihtsalt muutujasse
	var APIurl = "http://localhost:8080/chat/"+tuba
	//
	//fetch teeb päringu serverisse (meie defineeritud aadress)
	var request = await fetch(APIurl)
	//jason() käsk vormindab meile data mugavaks jsoniks
	var json = await request.json()
	// document.querySelector('#jutt').innerHTML = JSON.stringify(json)
	//kuva serverist saadud info HTMLis
	document.querySelector('#jutt').innerHTML = ""
	var sonumid = json.messages

	while(sonumid.length > 0) {
		var sonum = sonumid.shift()
		// console.log(sonum)
		// lisa HTMLi #jutt sisse sonum.message
		document.querySelector('#jutt').innerHTML += "<p>"+"<img src="+sonum.url+" width=30> " +sonum.user + " ütleb: " + sonum.message + "</p>"
	}

	window.scrollTo(0, document.body.scrollHeight);
}
//uuendab sõnumeid iga sekund. 1000 on sekund
setInterval(refreshMessages, 1000)

//
document.querySelector('form').onsubmit = function(e) {
	event.preventDefault()
	var username = document.querySelector('#username').value
	var message = document.querySelector('#message').value
	var url = document.querySelector('#url').value
	document.querySelector('#message').value = " " //tühjenda input

	// console.log(username, message)
	// document.querySelector('#jutt').innerHTML += "<br>" + username + " kirjutas: " + message


    var tuba = document.querySelector("#room").value
	//post päring postitab uue andmetüki serverisse
	var APIurl = "http://localhost:8080/chat/"+tuba+"/new-message"; //serveri poolt antud url
	fetch (APIurl, {
		method: "POST",
		body: JSON.stringify({user: username, message: message, url: url}),
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/json'
		}
	})

}