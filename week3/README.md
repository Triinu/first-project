# lopuprojekt

1. Eesmärk

Mäng: hobusteralli
Mängija panustab valitud hobusele rahaühikud, kogub raha, võib osta endale hobune. Panusta ja võida!

2. User stories
- Läheb veebilehele
- Logib sisse - programm kontrollib, kas see user olemas,
    -  kui jah, siis atribuutide leht
    -  keu ei, siis "viga, kasutajat ei ole, registreeri?"
- Kui registreerib
    -  programm kontrollib, kas on see user olemas
    -   kui ei, siis atribuutide lehele, user lisatud andmebaasi
    -   kui jah, siis "viga, user olemas"

- Näeb oma atribuudid - kontoseis (alguses mitte null), panustamiste-võistluste ajalugu, hobuste valik, võimalus valida uus mäng
        - hobuste valik: nimekiri, igal hobusel on link tema andmetele
        - kontoseis: andmebaasist number, uueneb pärast iga mängu
        - ajalugu: uueneb pärast iga mängu
        - uus mäng: algandmed olemas

- Uue mängu alustamiseks, valib hobune, mängijate arv, panustamise summa, panuse suurus, rada pikkus
- Vajutab nuppu "mängi"
- Mäng toimub
    -  on näha valitud hobuse nimi, aeg ja läbitud distants
- Lõpus ilmub võistlejate nimekiri, kõik kohad selguvad
- kontole kas + või - raha, sõltuvalt valitud tingimustest
- Nupust vajutades läheb tagasi esilehele (atribuudid)


NICE TO HAVE
- Valib kas random mäng, või valib teisi mängijaid




