package ee.valiit.kasutajatelist;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
public class APIController {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @PostMapping("/list")
    public void handleNewListItem(@RequestBody Kasutaja kasutaja) {
        System.out.println("handleNewListItem toimib");
        System.out.println("Kasutaja nimi: " + kasutaja.getNimi());
        String sqlKask = "INSERT INTO kasutajad (nimi, vanus) " +
                "VALUES ('"+kasutaja.getNimi()+"', "+kasutaja.getVanus()+");";
        jdbcTemplate.execute(sqlKask);
        System.out.println("Sisestamine õnnestus!");
    }
}
