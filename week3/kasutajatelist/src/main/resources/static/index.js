var url = "http://localhost:8080/list"
document.querySelector('#form1').onsubmit = function(e){
    console.log("onsubmit toimib");
    e.preventDefault();  //siis ta ei refreshi automaatselt
    var nimi = document.querySelector("#nimi").value
    var vanus = document.querySelector("#vanus").value

    fetch(url, {
        method: "POST",
        body: JSON.stringify({nimi, vanus}),
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    })

}